# Outils pour s'organiser etc

## Résumé

### Compte Art Liquide 
#### Création 
Demander à une personne du hacker space
#### Gestion
Pour les admins https://neth.artliquide.org:9090
Pour les autres https://neth.artliquide.org/user-settings

### Matrix 
#### Client web préconfiguré
https://element.artliquide.org/
#### Adresse Matrix automatique
De la forme `@pseudo:artliquide.org`
#### Discussion de groupe du Hack Liquide
Lien direct https://matrix.to/#/!GgqWZkBIlpaqBTEmeS:artliquide.org?via=artliquide.org&via=matrix.org&via=matrix.asso-fare.fr

### Email
#### Client web préconfiguré
Directement : https://mail.artliquide.org
Via NextCloud : https://cloud.artliquide.org/apps/roundcube/
#### Adresse email automatique
De la forme `pseudo@artliquide.org`

### Site wordpress
#### Lien public
https://artliquide.org
#### Gestion
https://artliquide.org/wp-login.php

### Stockage dans le cloud
https://cloud.artliquide.org/apps/files/

### Gestionnaire de budget
#### L'application dans une page web
https://cloud.artliquide.org/apps/cospend/
#### Les dépenses du hackspace
Dans les "projets" cospend, il faut choisir le projet "Hackerspace".

## Explications et détails

### Services en ligne

Nous disposons d'un serveur qui héberge différents logiciels qui fournissent divers services directement accesibles depuis une page web. Pour utiliser ces services, il faut se connecter sur ces différentes pages.

Nous avons un système qui fait qu'un unique compte Art Liquide (c'est à dire un pseudo et un mot de passe) permet d'utiliser tous les services hébergés sur notre serveur. Lorsque une personne dispose d'un compte Art Liquide, c'est comme si elle disposait automatiquement d'un compte sur chacun des services en ligne. Elle peut donc se connecter à ces différents services en utilisant toujours avec le même pseudo et le même mot de passe.

Chacun·e peut demander la création d'un compte Art Liquide à n'importe quelle personne du collectif du hackerspace. Si un·e administrateur·rice du serveur Art Liquide est disponible, la création de compte est instanné.

Les utilisateur·rice·s qui ne sont pas administrateur·rice·s peuvent accéder aux réglages de leur compte Art Liquide (notamment pour changer le mot de passe) ici : https://neth.artliquide.org/user-settings

Les administrateur·rice·s peuvent accéder à la gestion des comptes utilisateur·rice·s à cette adresse : https://neth.artliquide.org:9090

Les différents comptes Art Liquide peuvent disposer de "droits" différents (possibilité technique d'inviter d'autres personnes, possibilité technique de modifier le blog d'Art Liquide etc).

Les services en ligne accessibles à toustes sont présentées sur une page du site d'Art Liquide https://artliquide.org/services/ et aussi ci-dessous.

#### Une instance Matrix

Adresse Matrix créé automatiquement sous la forme `@pseudo:artliquide.org`

Matrix est un protocole de messagerie instantanné (comme Signal ou WhatsApp).

##### Matrix fonctionne un peu comme les emails.

Quelque soit votre adresse email (gmail, outlook, hotmail, riseup...), chacun·e peut envoyer et recevoir des emails d'autres adresses email, quel qu'elles soient (gmail, outlook, hotmail, riseup...).

De la même façon, chacun·e peut se connecter à son adresse Matrix pour échanger avec d'autres utilisateur·ice·s Matrix, quelque soit leur adresse (@simone:artliquide.org peut par exemple discuter avec @gaston:fuz.org),

Par ailleurs, vous pouvez accéder à votre boîte de réception et envoyer des messages depuis une multitude de logiciels différents. Par exemple si vous avez une adresse gmail, vous pouvez accéder à Gmail sur un site web ou alors avec Outlook, ThunderBird, une application pour téléphone etc.

Il en va de même pour Matrix.
Notre serveur héberge une page web qui permet de se connecter à Matrix le plus facilement du monde https://element.artliquide.org/
On peut aussi utiliser Matrix avec un logiciel sur ordinateur ou une application sur téléphone par exemple.    

##### Mais c'est différent des emails.

Contrairement aux emails, une personne qui dispose d'un compte Matrix peut non seulement échanger avec une autre personne qui dispose d'un compte Matrix mais aussi potentiellement avec des personnes qui utilisent Signal, Telegram, Mastodon, PeerTube...

##### Un protocole décentralisé.

Gmail par exemple communique la totalité des emails qui passent par ses serveurs à la NSA américaine.

WhatsApp prétend que les messages sont chiffrés (donc illisibles par quelqu'un d'autre que leur destinataire). Mais WhatsApp n'est pas open source. Cela signifie qu'il n'y a aucun moyen de savoir ce que Facebook (le propriétaire de WhatsApp) fait réellement avec nos messages lorsque nous les envoyons. Rien ne nous empêchent de croire que Facebook enregistre par exemple une copie de chaque message que nous envoyons.

Signal est open source et la communauté mondial des geeks ont vérifié que les messages sont bien chiffrés de bout en bout. Mais Signal nécessite de se connecter avec un numéro de téléphone. Dans une discussion de groupe dans laquelle un inconnu n'aurait aucun mal à se glisser, l'identité réelle des personnes qui discutent est donc très facile à obtenir.

Les comptes Matrix (qui ne nécessite ni numéro de téléphone ni autre moyen de connaître l'identité réelle de la personne) sont créés sur des serveurs que des personnes hébergent chez elles, dans des clouds... ou dans des hackerspace. Ces serveurs peuvent communiquer entre eux. Mais lorsque deux utilisateur·rice·s de notre instance Matrix Art Liquide se parlent directement entre elleux, les messages ne transitent par aucun serveur que nous ne contrôlions pas physiquement.

##### Des groupes de discussion thématiques

On peut créer des groupes (comme sur Signal par exemple).

Le groupe thématique du Hack Liquide est accessible directement en cliquant sur le lien suivant pour celleux qui utilisent le client web préconfiguré https://matrix.to/#/!GgqWZkBIlpaqBTEmeS:artliquide.org?via=artliquide.org&via=matrix.org&via=matrix.asso-fare.fr 

Sinon dans la plupart des logiciels avec lesquels une personne peut accèder à Matrix, il y a un bouton pour rechercher des "salons" dans une instance donnée. On peut facilement retrouver "Hack Liquide - Général" dans l'instance artliquide.org. 

#### Un site public wordpress pour Art Liquide

Un site https://artliquide.org est accessible à toustes.

Les personnes qui disposent d'un compte avec les droits nécessaires peuvent accéder à l'interface de gestion de ce site (créer du contenu, modifier les pages...): https://artliquide.org/wp-login.php

#### Un espace de stockage individuel dans le cloud

https://cloud.artliquide.org/apps/files/

#### Un email accessible sur une page web

Adresse email créée automatiquement sous la forme `pseudo@artliquide.org`

Chacun·e peut accéder à sa boîte mail directement sur une page web : https://cloud.artliquide.org/apps/roundcube/

#### Un gestionnaire de budget / dépenses

https://cloud.artliquide.org/apps/cospend/

##### Les dépenses liées au Hack Liquide en particulier

Rappel de la politique: les gens avancent ce qui qu'ils veulent et ensuite on voit à l'unanimité ce qu'on remboursera (et il faut déterminer quoi, qui et à quelle hauteur).

Comment on le met en place ?

- Un équivalent de Tricount sur Nextcloud #PierreEstUnForceur https://apps.nextcloud.com/apps/cospend ->  Pierre le met sur NextCloud parce que c'est auto-hebergé (les données restent sur notre serveur et pas chez un hébergeur privé) et que c'est exportable en un clic (si on est pas content on sort les données sous forme de tableau)
- Concrètement, chaque personne qui a un compte sur le nextcloud d'Art Liquide peut indiquer ses dépenses (indiquer "pour qui : moi seulement") et ensuite on peut discuter de ce qui peut être pris en charge collectivement et alors là on indique "pour qui : toustes"
- On peut aussi indiquer qui est ok quoi qu'il arrive pour partager quelle somme (genre Zef et Elie sont ok pour partager la puce 4G)
- Le soft fait des calculs

- On crée et commence à remplir le projet "hackerspace" dans cospend
https://cloud.artliquide.org/apps/cospend/
