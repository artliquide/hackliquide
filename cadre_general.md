# Cadre général de Hack Liquide

Ce cadre général doit être le plus claire possible et le plus accessible possible à toustes celleux qui participent ou souhaiteraient participer à Hack Liquide.

## Comportement

Il y a plusieurs points que chacun·e doit essayer de respecter dans le cadre des activités en lien avec Hack Liquide. Chacun·e est légitime aussi à essayer de les faire respecter (rappeler ces points à celleux qui ne les respecterai pas par exemple). Chacun·e peut sollicier de l'aide pour essayer de les faires respecter.

- Respecter et valoriser le consentement éclairé de toustes dans tous les domaines. Cela implique notamment :

  - Ne pas donner de conseil non-sollicité ou imposer son aide aux autres personnes. Accepter qu'une personne ne souhaite ni conseil ni aide à un moment donné sans pour autant fermer la porte à ce qu'elle demande un conseil ou de l'aide à un autre moment.

  - Ne pas faire de blagues à caractère sexuel à une ou des personnes sans savoir si elle sont d'accord avec ça (donc notamment aux inconnu·e·s).

## Participant·e·s

- La participation à Hack Liquide est volontaire et libre (donc gratuite).

- Elle est soumise au respect du cadre général de Hack Liquide.

- Une personne peut tout à faire ne plus être la bienvenue à participer à Hacl Liquide en général et à venir au hackerspace d'Art Liquide sur décision du noyau.

- Il existe un groupe surnomé ici "le noyau" qui est constitué de personnes qui se sont impliquées dans le projet dès le départ ou qui ont été invitées par celles-ci à faire partie du noyau.

- Le noyau est constitué pour l'instant de six personnes, dont :
  - Pierre
  - Elie
  - Hadrien
  - Rozlav
  - Sabrina
  - Zef

- Une personne du noyau peut aussi décider de quitter le noyau définitivement bien sûr mais aussi de suspendre temporairement son implication et de laisser carte blanche au reste du noyau (c'est comme si iel était favorable à tout jusqu'à son retour). Si une personne ne vient plus physiquement au hackerspace pendant une certaine durée (par exemple 1 mois), alors c'est comme si elle se suspendait jusqu'à son retour (à moins qu'elle ne demande explicitement au reste du noyau à ne pas être suspendue malgré une absence un peu longue et que le reste du noyau est d'accord).

## Activités

- Les activités des personnes qui participent à Hack Liquide, individuellement ou en groupe, s'effectuent dans le respect du cadre général de Hack Liquide.

- Au delà de ça, elles s'organisent, se mènent et cohabitent en toute autonomie. Cela concerne les sujets, le planning et les modalités de fonctionnement à l'intérieur d'une activité ou d'un groupe par exemple.

- Le noyau vie à favoriser des pratiques démocratiques horizontales, potentiellement asynchrones, entre toustes les participant·e·s (organisation d'AG des différentes activités, par exemple).

## La hackerspace d'Art Liquide

- Pour modifier un élément matériel du hackerspace d'Art Liquide (de la déco à la structure-bois en passant par les outils numériques)
  - Une ou plusieurs personnes du noyau n'ont pas besoin de la validation _a priori_ des autres personnes du noyau.

  - Les autres participant·e·s à Hack Liquide peuvent demander à une personne du noyau si celle-ci est d'accord pour être garante.

  - A moins que la modification en question ne soit réellement évidente à observer, les personnes du noyau qui effectuent une modification ou bien qui se portent garantes tiennent au courant les autres personnes du noyau.

- Chaque membre du noyau peut contester _a posteriori_ une modification qui a été effectuée.
  
  - Chacun·e s'engage à écouter et prendre en compte cette position. Un temps d'élaboration collective peut être nécessaire pour aboutir à une position commune.

  - La personne du noyau qui a effectué la modification ou bien la personne qui a accepté de se porter garante doit donc pouvoir revenir sur cette modification, tout remettre en état comme c'était avant ou participer à mettre en place ce qui a été décidé collectivement.  

Par conséquent, plus une éventuelle modification semble difficilement réversible et plus ses conséquences semblent importantes, plus il est important de s'assurer _a priori_ de l'accord des autres personnes du noyau. 

## Les sous

- Chacun·e peut apporter au hackerspace d'Art Liquide ce qu'il souhaite et dépenser ce qu'il souhaite pour cela.

- Le noyau détermine collectivement, a priori ou a posteriori, ce qui peut être pris en charge ou non par le groupe. Si une personne dépense sans avoir solliciter l'avis du noyau a priori, elle accepte de s'exposer à ce que le noyau décide de ne pas prendre en charge cette dépense.

- Ce qui est pris en charge par le groupe est centralisé dans un document. Lorsqu'on bénéficie de rentrées d'argent (boîte à prix libre essentiellement probablement), ces dépenses sont remboursées par ordre de priorité à celleux qui ont le plus dépensées. _Exemple : si le noyau décide que 15€ dépensé par X et 10€ dépensés par Y seront pris en charge et si on constante qu'on a obtenu 11€ dans la boîte à prix libre, alors X récupère 8€ et Y récupère 3€ avec que ce qui reste à rembourser à X et Y s'équilibre._

## Evolution du cadre

- Chacun·e peut formuler et soumettre au noyau un ajout, modif ou suppression d'un point de ce cadre général.
  
- Si et seulement si cette proposition recueille l'unanimité, alors on peut l'inscrire à l'écrit tel quel dans ce cadre général. Pour recueuillir l'avis de toustes, n'importe quel canal peut être utilisé et ça peut être complétement asynchrone.

- En revanche, tant que la proposition ne recueille pas l'unanimité, elle ne s'applique pas. Celles et ceux qui souhaitent que ça avance peuvent proposer des temps d'élaboration collective pour trouver quelque chose qui va à toustes et le soumettre de nouveau à toustes et ainsi de suite jusqu'à ce qu'il y ait unanimité. On compte sur celles et ceux qui ne sont pas d'accord avec une proposition pour jouer le jeu de l'élaboration collective et ne pas être dans une posture de rejet non constructive.
