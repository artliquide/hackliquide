# Matériel

## Ce qu'on a

### Clés 
On dispose de 4 clés de la porte d'AL. Pierre et Elie en ont une. Zef en a actuellement deux.

### Ce qui est utilisé 

#### Cybercafé

- "Patrick" OK (Dell Optiplex 2Go RAM 80Go HDD VGA) + écran clavier souris
- "Jeanne" OK (Dell Precision390 4Go de RAM 250 Go HDD) + écran clavier souris
- "Laura" OK (Dell Pentium 4 2Go RAM) + écran clavier souris
- "Jacqueline" OK (iMax avec Ubuntu dessus)

#### Bibliothèque de prêt de Matériel

- un laptop, marqué "Satellite pro" non-nommée (ubuntu mis à jour le 17/11/21)

#### Matériel HS à réparer

- Dell PrecidionT3500 ??? démarre pas trop (son de la carte mère a diagnostiquer) VGA DVI
- NEC Pentium 4HL vieux VGA (amené par Dyron) (l'image VGA ondule un peu)

- Laptop blanc manque au moins alim (on a testé toutes les alim non universelles qu'on avait) et RAM (qu'on a)
- Laptop gris manque au moins alim (idem)
- MacBook manque au moins alim

#### Autres matériels

- 2 souris usb (dont une apple un peu chiante)
- 1 clavier usb pas ouf
- 1 clavier PS2 pas mal

## Ce dont on a besoin

- Des multiprises.
- Internet !
- Des lampes !
- du câble électrique 1,5 mm², 3g, 50 m si on veut être large, pour câbler le hackerspace
- des prises murales en bloc, contenant 1 ou 2 prises
- du câble RJ45 pour faire baie de brassage - hackerspace et baie de brassage - antenne 4G, si possible monobrin F/UTP voire SF/UTP (si on passe à côté de câbles électriques)
- 4 prises RJ45 femelles
tour old school manque HDD et ram (qu'on a trouvé) mais même branché l'alim ne démarre pas

- Peut-être fait ou non depuis, on ne sait plus :
Ajout de la RAM dans les tours du cybercafé
Ajout du disque PATA "Laura" dans la tour "Laura" et le formatter pour qu'il y ait un peu plus de stockage
