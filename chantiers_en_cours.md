# Chantiers en cours

## Accès internet pour le bâtiment
Objectif : un dispositif pérenne et avec un bon débit pour tout le bâtiment. Ca sera aussi un grand facilitateur pour les activités du hackerspace.

### Installer en urgence un dispositif 4G
On dispose de tout le matériel nécessaire (Pierre a la plupart des éléments (parabole, routeur, bornes wifi)) pour apporter Internet à tout le bâtiment (environ 300Mb/s à se partager), en wifi (avec 4 bornes placés stratégiquement) et en Ethernet (priorité au hackerspace, ensuite des pièces communes et des chambres mais une partie du bâtiment est déjà câblée).
Il manque uniquement :
- un touret de câble RJ45 (longueur suffisante) -> Il y en a un chez Léo -> Zef lui envoie un message et il faudra passer le prendre s'il dit oui
- on a besoin d'une puce 4G -> Pierre achète un abonnement à 16€ chez Free à son nom + 10€ pour la SIM, Zef Rozlav Elie et Pierre sont ok pour partager cette somme quoi qu'il arrive (sans demander de remboursement à personne ou alors à prix libre) -> On propose d'offrir la 4G aux habitants (notamment car on paye pas la participation demandée aux habitants)
- l'abonnement est pris, la SIM va arriver chez Pierre
- on a besoin de faire le tour du bâtiment pour voir ce qu'il y a à câbler et où

### Faire venir la fibre
- demander à l'opérateur sympa [Leonix](https://www.leonix.fr/) si ils peuvent fibrer la rue
- demander au contact de Rozlav (un militant qui travaille chez [Ielo](https://www.ielo.net/) qui peut nous apprendre et nous faire une ristourne) si il peut 1) nous raccorder 2) le faire 3) nous dire combien ça coûte -> Rozlav lui a donné l'adresse et poser les question 4) combien de temps d'abonnement (Pierre rappelle que MVMV est le nom de l'asso qui chapeaute les autres ici)

- si on fait ça, un pro ou ami de Roz fera le raccordement (en quelques heures max) jusqu'au bâtiment et/ou jusqu'à la baie de brassage
- le reste c'est comme pour la 4G, installation de câbles Ethernet et bornes wifi (sans toucher à l'installation dans le bâtiment qu'on avait faite pour la 4G)
- si la fibre fonctionne bien, on peut arrêter l'abonnement 4G (déjà parce que ça sera trop pour être utile mais aussi parce que la fibre est une infrastructure commune contrairement à la 4G qui représente un danger pour Internet (Zef souligne qu'il n'est pas d'accord avec ce second argument qu'il rapproche d'une croyance qu'une forme de consommation pourrait avoir un impact sur les pratiques des industriels mais il accepte largement le premier argument))

- ça ça va être beaucoup plus cher (150€ par mois environ), mais ça sera pris en charge par les habitants d'ici (Oli s'est engagé à cela si on arrive à installer la fibre)

### Plan de secours : 
Un plan ADSL de Pierre pas cher (si jamais la 4G et la fibre ne fonctionne pas, mais ça sera long à obtenir aussi).

## Bureau pour que le noyau et leurs invité.e.s puissent travailler sur ordis au chaud

C'est notamment une demande d'Elie et Pierre.

On a une pièce qui ferme à clé, qui est un peu isolée du bruit et du froid et dans laquelle il y a le chauffage de Zef (temporairement). Il y a du mobilier (un canapé temporaire, des bureaux, chaises et tables).

### Pour améliorer :

- Une meilleure isolation ?
- - Idéalement, Internet avec un méga débit accessible en Ethernet ou wifi depuis la pièce fermée du hackersapce.

## Cybercafé accès libre

Il y a maintenant 4 ordis avec écran clavier souris fonctionnels (des distributions linux) au cybercafé, avec les multiprises qui vont avec.
Zef et Pierre disposent du mot de passe sudo des sessions admins sur ces 4 machines.

Il y a un panneau lumineux qui dit "ouvert" et il y a une affichette qui explique qu'on peut utiliser les ordis librement.

Deux des quatre ordis disposent d'une antenne wifi et peuvent être utiliser avec un partage de connexion d'un smartphone.

### Pour améliorer

- Installer un système pour qu'un partage de connexion d'un smartphone puisse se "transformer" en Internet pour les deux autres machines qui ne sont pas équipés en matériel wifi. Pierre a bien commencé ce chantier-ci.
- Idéalement, Internet avec un méga débit accessible depuis toutes les machines sans intervention de l'utilisateur.rice (et des panneaux qui annoncent cela).

- Après une énième coupure de courant, la machine "Jeanne" ne boot plus (bloque pendant le démarrage d'Ubuntu). Réinstaller un OS dessus ?

## Atelier "grands débutants" ordi/smartphone

### Intérêt
- Ca permet à toustes de s'investir et de participer et de créer du lien et de faire de l'auto-formation entre les personnes et parce qu'il y a un besoin identifié (personnes précaires, groupe TDS, "mineurs non-accompagnés" etc).
- Comment faire en sorte que ça participe à la transformation radicale des rapports sociaux ?

### Pour améliorer

Pour que ça ait lieu, il nous faut :

- [x] installer les PC (même si ça sera des machines linux) : Elie est dispo
- Facultatif - avoir Internet mieux qu'en partage de connexion (déjà en cours, voir plus haut) 
- avoir un support (texte/image imprimable en flyer etc) pour présenter l'atelier et le mettre en ligne sur le Wordpress d'Art Liquide -> Zef ok pour s'y coller, à plusieurs c'est mieux (Elie pas chaud tout de suite mais dans l'absolu pourquoi pas)
- avoir un planning des activités du hackspace à mettre un truc sur wordpress + affichage sur le hackerspace (Zef est chaud pour le faire) + informer les habitants
- inviter des personnes à participer : (Elie pense pas être actif sur ça)
    - en parler aux MNA (en alphabétisation par exemple) avec Paris d'Exil -> Zef s'en occupe (Elie pense que ça stress de faire venir des personnes qui vivent à la rue et qu'il faut les faire sortir après et Pierre précise qu'il y aura toujours des gens au squat pour faire partir celleux qui veulent pas partir mais c'est vrai que c'est pas agréable) donc faudra penser dans cette comm a bien clarifié qu'iels pourront pas leur en parler
    - l'espace Aeri à Montreuil (https://www.helloasso.com/associations/aeri-montreuil/collectes/soutenir-une-utopie-reelle-soutenir-aeri), on peut aller y mettre un mot pour prévenir qu'on va faire -> Pierre est chaud d'y aller pour tester la cantine syrienne
    - en parler aux TDS du bus des femmes -> Zef s'en occupe
    - parler en AG des habitantes qu'on a ça qui commence
    - autres canaux ?

- Facultatif - avoir des supports de formation en ligne : Elie propose un outil pour les héberger et puis on peut giter un markdown pour collaborer sur l'écriture de supports
- animer la formation (sans forcément de préparation) : Elie est dispo, Zef aussi ; Pierre pas dispo

- Si on veut un créneau régulier, c'est chaud le mercredi soir pour Elie.

- Un atelier de prévu avec Will

## Bibliothèque de prêt de matériel

- Zef a commencé à retaper les laptops pour les stocker avec les périphériques nécessaires, un à un, pour pouvoir les prêter à des personnes qui souhaitent utiliser un laptop dans leurs espaces (mater un film, travailler dans un espace clos...). L'idée serait de pouvoir prêter le matériel en sachant à qui, comme ça quand il y a d'autres personnes qui demandent une machine et qu'on en a plus en stock, on fait revenir celles qui sont parties depuis le plus longtemps, par exemple.

#### Autres pistes et autres choses amorcées

### Bridge Matrix/Signal

Hadrien : "Avec Pierre on voulait faire un bridge entre signal et matrix, c'est à dire que les messages seraient transmis dans les deux canaux de communication"

### Améliorer le WP et le dynamiser

Impliquer des personnes d'AL hors de HL dedans.
Retoucher la page "services".

## Les activités
S'il y a des publics chauds, ça va nous motiver. 

### Grandes débutantes
Ateliers non-mixtes

### Git pour les non-développeurs
Git lab, pas de ligne de commande
Proposé par Elie

### Live code
Elie dit que c'est ce qui le motive le plus dans la vie. C'est une façon d'enseigner la programmation de manière créative.

Samedi il y a 15 jours le collectif "cookie party" a payé le Zorba (à Belleville) pour y faire un apéro + live code. 
Elie propose de leur proposer de se retrouver au HS pour apéro + live-code. On sait pas encore si on pourra inviter du public mais pourquoi pas.

Elie propose aussi de faire des ateliers d'initiation à la programmation par le live-coding, tout public intéressé (pour les curieux pas geeks mais ça reste un tout petit peu compliqué) avec une grosse vibe artistique.

### Co-working
Hebdomadaire.
Le vendredi après-midi.
Pierre peut ramener des happy devs.
On met la boîte prix libre.

### Portes ouvertes
Hebdomadaire.
Pierre propose apéro le vendredi soir parce que c'est moins festif que le samedi soir mais ça peut l'être.
Comment on communique ?

### Autres idées Zef
A discuter

Panneaux qui indiquent

Cyber café
Retape ordis (Hack écrire que dispo pour réparer les ordos et téléphone, les faire aller plus vite, les faire avoir accès à tel ou tel truc; notamment via du free softawre ou du crack)
Prêts dans le squat

Tract sur obsolescence programmé comme produit du capitalisme

Propagande en utilisant des moyens un peu spéciaux comme https://xkcd.com/1331/

### Activités avec le reset

### Débats / présentation de livres etc

Sabrina ?

## Autres pistes 

### Anciens hackerspace

1 - Un atelier méca / electronique pour celleux qui veulent bricoler / réparer leur matos (l'idée étant qu'on peut etre là soit de permanence soit sur demande pour accompagner la démarche de bricolage)
2 - Une activité de récup et de remise en état de matos pour le redistribuer a celleux qui en ont besoin (qu on peut faire avec les futurs utilisateurs de ce futur matos)
3 - Une salle «cybercafé» avec des machines (de récup qu on peut se permettre de perdre ou de se faire piquer) en acces libre (l'idée étant qu'on peut etre là soit de permanence soit sur demande pour accompagner la démarche pour inititier aux BABA de la manip d un ordi etc), avec un.des panneaux qui expliquent que si des personnes partent avec les machines, les autres y auront pas acces alors qu'on peut essayer de fournir des machines de récup a celleux qui en ont besoin hors du cybercafé
4 - Un accompagnement dans les démarches administratives (impossibles sans le net mais qui nécessite plus que le net, genre la langue et la compréhension de comment ca marche ou des vers qui se tourner pour avoir de l aide)
-> Toutes les démarches d'accompagnement proposées ci dessus (bricolage, récup/retap, administratif...) sont effectués dans le but de permettre aux personnes de s autonomiser un peu individuellement aussi dans ce domaine
5 - Des formations (un.e sachant.e et des apprenant.e.s) et autoformations collectives (personne sait mais découvre plus ou moins ensemble) sur d autres sujets liés aux technologies (donc pas forcément directement liés a la vie quotidienne comme les points 1 a 4 au dessus), pour répondre aux demandes des personnes etou qu on propose de notre propre initiativ
6 - Des ateliers sur les themes technos/jeuxvidéos/cinamé / confs / débats plus intellectuels / educ pop (au sens de l émancipation organisée, cf discussion sur l éduc pop originelle avant la récup par la socialdémocratie et le citoyennisme), et donc reflechir politiquement a comment s émanciper des technos ou grace aux technos
-> Toutes les choses proposées précédemment ça ouvert aussi en personnes extérieures aux batiments (a voir comment on organise ca, a qui on s adresse, comment etc).
-> Toutes les choses proposées précédemment seraient proposées aux personnes a prix libre et/ou une boite a don 100% facultatif ; l'argent récolté sert a financé les activités du HS). On peut si on le souhaite en filer une part aux habitants du batiment mais ils nous demanderont rien de plus que tous les services qu on leur rend deja (réseau et les autres activités du HS) // bien que ca puisse paraitre contradictoire de prendre des dons des habitants et de leur redonner des sous (autant le mieux c est que les habitants des bats comprennent bien que c est un vrai prix libre et qu ils peuvent donner zero).
7 - Un lieu QG ( = un bureau) pour une activité de dev (si possible en collectif / boite autogéré etc)
8 - Un lieu QG / bureau aussi pour *** c est a dire une application politique des reflexions qui naissent du HS sous forme de propagande / plaidoyer etc (contenus médiatiques notamment)
-> Ces deux derniers points sont plutot1 pour le collectif au coeur du HS (un mini collectif dans le grand collectif qu est l Oréal), un peu comme le fait que telle artiste a une piece qui lui sert d atelier a elle, c'est pas un soucis qu elle fasse aussi des trucs de son coté pas forcément 100% du temps tourné vers les autres.
Une affiche sur la porte d'entrée pour mutualiser les propositions d'ateliers etc (pour participer a réunir des personnes sur un thème pour qu'elles fassent des choses ensemble avec une impulsion de quelqu'un du collectif des zozos si besoin)

- Proposer à toustes de les aider à faire leurs sites vitrines perso (sous partie du site du lieu ou non).
